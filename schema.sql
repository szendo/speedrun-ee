CREATE TABLE game
(
  id    BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  title VARCHAR(255)           NOT NULL,
  image LONGBLOB
);

CREATE TABLE category
(
  id      BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  game_id BIGINT(20)             NOT NULL,
  name    VARCHAR(255)           NOT NULL,
  rules   TEXT                   NOT NULL,
  CONSTRAINT category_game_id_fk FOREIGN KEY (game_id) REFERENCES game (id)
);
CREATE INDEX category_game_id_fk ON category (game_id);

CREATE TABLE entry
(
  id          BIGINT(20) PRIMARY KEY  NOT NULL AUTO_INCREMENT,
  category_id BIGINT(20)              NOT NULL,
  username    VARCHAR(255)            NOT NULL,
  run_time    BIGINT(20)              NOT NULL,
  video_url   VARCHAR(255) DEFAULT '' NOT NULL,
  comment     TEXT                    NOT NULL,
  CONSTRAINT entry_category_id_fk FOREIGN KEY (category_id) REFERENCES category (id)
);
CREATE INDEX entry_category_id_fk ON entry (category_id);

CREATE TABLE user
(
  id       BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  username VARCHAR(255)           NOT NULL,
  password VARCHAR(44)            NOT NULL
);
CREATE UNIQUE INDEX user_username_uindex ON user (username);

CREATE TABLE role
(
  user_id BIGINT(20) NOT NULL,
  role    VARCHAR(5) NOT NULL,
  CONSTRAINT role_user_id_fk FOREIGN KEY (user_id) REFERENCES user (id)
);
CREATE INDEX role_user_id_fk ON role (user_id);
