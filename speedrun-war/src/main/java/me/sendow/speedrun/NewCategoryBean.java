package me.sendow.speedrun;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.CategoryModel;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.GameService;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class NewCategoryBean implements Serializable {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient CategoryService categoryService;

    private CategoryModel model;
    private Game game;

    public String init(long gameId) {
        this.model = new CategoryModel();
        this.game = gameService.findById(gameId);
        return "/category/new?faces-redirect=true";
    }

    public String create() {
        final Category category = model.toEntity();
        categoryService.createForGame(category, game.getId());
        try {
            return String.format("/game?gameId=%d&categoryId=%d&faces-redirect=true", game.getId(), category.getId());
        } finally {
            game = null;
            model = null;
        }
    }

    public CategoryModel getModel() {
        return model;
    }

}
