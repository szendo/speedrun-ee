package me.sendow.speedrun;

import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.GameModel;
import me.sendow.speedrun.service.GameService;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class EditGameBean implements Serializable {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    private Long id;
    private GameModel model;

    public String init(long gameId) {
        final Game game = gameService.findById(gameId);
        id = game.getId();
        model = new GameModel();
        model.setTitle(game.getTitle());
        return "/game/edit?faces-redirect=true";
    }

    public String edit() {
        final Game game = gameService.update(id, model.getTitle(), model.getImageData());
        try {
            return String.format("/game?gameId=%d&faces-redirect=true", game.getId());
        } finally {
            id = null;
            model = null;
        }
    }

    public String delete() {
        gameService.delete(id);
        try {
            return "/index?faces-redirect=true";
        } finally {
            id = null;
            model = null;
        }
    }

    public GameModel getModel() {
        return model;
    }

}
