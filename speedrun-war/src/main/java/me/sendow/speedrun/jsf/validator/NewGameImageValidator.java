package me.sendow.speedrun.jsf.validator;

import javax.faces.validator.FacesValidator;

@FacesValidator("me.sendow.speedrun.jsf.validator.NewGameImageValidator")
public class NewGameImageValidator extends AbstractGameImageValidator {
    public NewGameImageValidator() {
        super(false);
    }
}
