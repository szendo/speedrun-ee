package me.sendow.speedrun.jsf.validator;

import javax.faces.validator.FacesValidator;

@FacesValidator("me.sendow.speedrun.jsf.validator.EditGameImageValidator")
public class EditGameImageValidator extends AbstractGameImageValidator {
    public EditGameImageValidator() {
        super(true);
    }
}
