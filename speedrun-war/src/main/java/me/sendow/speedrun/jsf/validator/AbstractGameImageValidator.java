package me.sendow.speedrun.jsf.validator;

import me.sendow.speedrun.util.ImageUtil;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;

abstract class AbstractGameImageValidator implements Validator {

    private final boolean allowNull;

    AbstractGameImageValidator(boolean allowNull) {
        this.allowNull = allowNull;
    }

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        if (value != null) {
            if (!ImageUtil.validateUploadedPart((Part) value)) {
                final FacesMessage facesMessage = new FacesMessage("invalid image");
                facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(facesMessage);
            }
        } else if (!allowNull) {
            final FacesMessage facesMessage = new FacesMessage("missing image");
            facesMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(facesMessage);

        }
    }
}
