package me.sendow.speedrun.jsf.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("me.sendow.speedrun.jsf.converter.RunTimeConverter")
public class RunTimeConverter implements Converter {
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        throw new RuntimeException("Not implemented");
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        long runTime = (long) value;
        StringBuilder result = new StringBuilder();

        if (runTime >= 1000 * 60 * 60) {
            result.append(String.format("%d:%02d:%02d",
                    runTime / (1000 * 60 * 60),
                    runTime / (1000 * 60) % 60,
                    runTime / 1000 % 60));
        } else {
            result.append(String.format("%d:%02d",
                    runTime / (1000 * 60),
                    runTime / 1000 % 60));
        }

        if (runTime / 10 % 100 != 0) {
            result.append(String.format(".%02d",
                    runTime / 10 % 100));
        }

        return result.toString();
    }
}
