package me.sendow.speedrun.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class VideoEmbedUtil {

    private static final Pattern YOUTUBE_PATTERN = Pattern.compile("https?://www.youtube.com/watch\\?v=([^&]+)");
    private static final Pattern TWITCH_PATTERN = Pattern.compile("https?://www.twitch.tv/[^/]+/v/(\\d+)");

    private VideoEmbedUtil() {
    }

    public static String getEmbedUrl(String url) {
        if (url == null) return null;

        Matcher matcher = YOUTUBE_PATTERN.matcher(url);
        if (matcher.matches()) {
            return "https://www.youtube.com/embed/" + matcher.group(1);
        }

        matcher = TWITCH_PATTERN.matcher(url);
        if (matcher.matches()) {
            return "http://player.twitch.tv/?autoplay=false&video=v" + matcher.group(1);
        }

        return url;
    }

}
