package me.sendow.speedrun.util;

import org.imgscalr.Scalr;

import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public final class ImageUtil {

    private ImageUtil() {
    }

    public static boolean validateUploadedPart(Part part) {
        try (final InputStream inputStream = part.getInputStream()) {
            final BufferedImage originalImage = ImageIO.read(inputStream);
            if (originalImage == null) {
                return false;
            }
            originalImage.flush();
            return true;
        } catch (IOException e) {
            throw new RuntimeException("Cannot validate uploaded image", e);
        }
    }

    public static byte[] processUploadedPart(Part part) {
        try (final InputStream inputStream = part.getInputStream()) {
            final BufferedImage originalImage = ImageIO.read(inputStream);
            if (originalImage == null) {
                return new byte[0];
            }
            final BufferedImage resizedImage = Scalr.resize(originalImage, Scalr.Mode.FIT_TO_WIDTH, 360, 550);
            originalImage.flush();
            final ByteArrayOutputStream result = new ByteArrayOutputStream();
            ImageIO.write(resizedImage, "png", result);
            resizedImage.flush();
            return result.toByteArray();
        } catch (IOException e) {
            throw new RuntimeException("Cannot process uploaded image", e);
        }
    }

}
