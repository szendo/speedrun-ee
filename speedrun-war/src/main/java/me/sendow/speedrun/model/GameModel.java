package me.sendow.speedrun.model;

import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.util.ImageUtil;

import javax.servlet.http.Part;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class GameModel extends AbstractModelBase<Game> {

    @NotNull
    @Size(min = 1, message = "this field cannot be blank")
    private String title;

    private byte[] imageData;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Part getImage() {
        return null;
    }

    public void setImage(Part image) {
        if (image == null || image.getSize() == 0) {
            this.imageData = null;
            return;
        }
        this.imageData = ImageUtil.processUploadedPart(image);
    }

    public byte[] getImageData() {
        return imageData;
    }

    @Override
    public Game toEntity() {
        final Game game = new Game();
        game.setTitle(getTitle());
        if (getImageData() != null) {
            game.setImage(getImageData());
        }
        return game;
    }

}
