package me.sendow.speedrun.servlet;


import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.service.GameService;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

@WebServlet("/game/image/*")
public class GameImageServlet extends HttpServlet {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            final long id = Long.parseLong(req.getPathInfo().substring(1));
            final Game game = gameService.findById(id);
            if (game != null) {
                final ServletOutputStream outputStream = resp.getOutputStream();
                if (game.getImage() == null) {
                    try (final InputStream inputStream = getClass().getResourceAsStream("/default.png")) {
                        byte[] buf = new byte[256 * 1024];
                        int len;
                        while (-1 != (len = inputStream.read(buf))) {
                            outputStream.write(buf, 0, len);
                        }
                    }
                } else {
                    outputStream.write(game.getImage());
                }
                outputStream.close();
            } else {
                resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            }
        } catch (NumberFormatException e) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }

}
