package me.sendow.speedrun.ws;

import me.sendow.speedrun.service.GameService;

import javax.inject.Inject;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService
public class GameCatalog {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    public List<Game> listGames() {
        return gameService.listAll().stream()
                .map(g -> new Game(g.getId(), g.getTitle()))
                .collect(Collectors.toList());
    }

}
