package me.sendow.speedrun;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;
import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.EntryService;
import me.sendow.speedrun.service.GameService;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Named
@SessionScoped
public class GameBean implements Serializable {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient CategoryService categoryService;

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient EntryService entryService;

    private long gameId;
    private Game game;
    private List<Category> categories;

    private Long categoryId;
    private Category category;
    private List<Entry> entries;

    public void init(Long gameId, Long categoryId) throws IOException {
        if (gameId == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/index.jsf");
        } else {
            setGameId(gameId);
            if (categoryId != null) {
                setCategoryId(categoryId);
            }
        }
    }

    public long getGameId() {
        return gameId;
    }

    private void setGameId(long gameId) {
        this.gameId = gameId;
        this.game = gameService.findById(gameId);
        this.categoryId = null;
        this.category = null;
        this.entries = null;
        if (game != null) {
            this.categories = categoryService.listForGame(game.getId());
        } else {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect("/game/new.jsf");
            } catch (IOException e) {
                throw new RuntimeException("Cannot redirect", e);
            }
        }
    }

    public Game getGame() {
        return game;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    private void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
        this.category = categoryService.findById(categoryId);
        if (category != null) {
            this.entries = entryService.listForCategory(categoryId);
        } else {
            this.entries = Collections.emptyList();
        }
    }

    public Category getCategory() {
        return category;
    }

    public List<Entry> getEntries() {
        return entries;
    }

}
