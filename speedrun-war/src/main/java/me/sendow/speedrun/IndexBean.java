package me.sendow.speedrun;

import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.service.GameService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@ApplicationScoped
public class IndexBean {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    public List<Game> getGameList() {
        return gameService.listAll();
    }

}
