package me.sendow.speedrun;

import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.GameModel;
import me.sendow.speedrun.service.GameService;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class NewGameBean implements Serializable {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    private GameModel model = new GameModel();

    public void init() {
        model = new GameModel();
    }

    public String create() {
        final Game game = gameService.create(model.toEntity());
        try {
            return String.format("/game?gameId=%d&faces-redirect=true", game.getId());
        } finally {
            model = null;
        }
    }

    public GameModel getModel() {
        return model;
    }

}
