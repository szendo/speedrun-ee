package me.sendow.speedrun;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Named
@RequestScoped
public class LoginBean {

    @Inject
    private HttpServletRequest request;

    private String username;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isLoggedIn() {
        return request.isUserInRole("**");
    }

    public boolean isAdmin() {
        return request.isUserInRole("admin");
    }

    public String login() {
        if (request.getAuthType() != null) {
            return "/index?faces-redirect=true";
        }
        try {
            request.login(username, password);
            return "/index?faces-redirect=true";
        } catch (ServletException e) {
            e.printStackTrace();
            return "/login?faces-redirect=true&loginError=1";
        }
    }

    public String logout() throws ServletException {
        request.logout();
        return "/index?faces-redirect=true";
    }

}
