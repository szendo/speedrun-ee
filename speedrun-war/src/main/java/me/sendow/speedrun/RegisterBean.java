package me.sendow.speedrun;

import me.sendow.speedrun.entity.User;
import me.sendow.speedrun.service.UserService;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Named
@RequestScoped
public class RegisterBean {

    @Inject
    private UserService userService;

    @Inject
    private HttpServletRequest request;

    @NotNull
    @Size(min = 1, max = 255)
    private String username;
    @NotNull
    @Size(min = 1, max = 1024)
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String register() {
        if (request.getAuthType() != null) {
            return "/index?faces-redirect=true";
        }
        final User newUser = userService.create(username, password);
        if (newUser != null) {
            return "/index?faces-redirect=true";
        } else {
            final FacesContext facesContext = FacesContext.getCurrentInstance();
            ((UIInput) facesContext.getViewRoot().findComponent("registerForm:username")).setValid(false);
            final FacesMessage errorMessage = new FacesMessage("username already in use: " + username);
            errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
            facesContext.addMessage("registerForm:username", errorMessage);
            facesContext.validationFailed();
            return null;
        }

    }

}
