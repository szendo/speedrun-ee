package me.sendow.speedrun;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;
import me.sendow.speedrun.model.EntryModel;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.EntryService;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class EditEntryBean implements Serializable {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient CategoryService categoryService;

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient EntryService entryService;

    private EntryModel model;
    private Category category;

    public String init(long categoryId) {
        this.model = new EntryModel();
        this.category = categoryService.findById(categoryId);
        final String username = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
        final Entry entry = entryService.findByUsernameAndCategory(categoryId, username);
        if (entry != null) {
            model.setComputedRunTime(entry.getRunTime());
            model.setVideoUrl(entry.getVideoUrl());
            model.setComment(entry.getComment());
        }
        return "/entry/edit?faces-redirect=true";
    }

    public String createOrEdit() {
        final String username = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
        final Entry entry = entryService.findByUsernameAndCategory(category.getId(), username);
        if (entry != null) {
            entryService.update(entry.getId(), model.getComputedRunTime(), model.getVideoUrl(), model.getComment());
        } else {
            model.setUsername(username);
            entryService.createForCategory(model.toEntity(), category.getId());
        }
        try {
            return String.format("/game?gameId=%d&categoryId=%d&faces-redirect=true", category.getGame().getId(), category.getId());
        } finally {
            model = null;
            category = null;
        }
    }

    public EntryModel getModel() {
        return model;
    }
}
