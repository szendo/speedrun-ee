package me.sendow.speedrun;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.EntryService;
import me.sendow.speedrun.util.VideoEmbedUtil;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@Named
@SessionScoped
public class EntryBean implements Serializable {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient CategoryService categoryService;

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient EntryService entryService;

    private long entryId;
    private Entry entry;
    private long rank;
    private String embedUrl;

    public void init(Long entryId) throws IOException {
        if (entryId == null) {
            FacesContext.getCurrentInstance().getExternalContext().redirect("/index.jsf");
        } else {
            setEntryId(entryId);
        }
    }

    public long getEntryId() {
        return entryId;
    }

    private void setEntryId(long entryId) {
        this.entryId = entryId;
        entry = entryService.findById(entryId);
        if (entry != null) {
            rank = entryService.getRank(entryId);
            embedUrl = VideoEmbedUtil.getEmbedUrl(entry.getVideoUrl());
        } else {
            rank = 0;
            embedUrl = null;
        }
    }

    public String delete() {
        final Category category = categoryService.findByEntry(entryId);
        entryService.delete(entryId);
        return String.format("/game?gameId=%d&categoryId=%d&faces-redirect=true", category.getGame().getId(), category.getId());
    }

    public Entry getEntry() {
        return entry;
    }

    public long getRank() {
        return rank;
    }

    public String getEmbedUrl() {
        return embedUrl;
    }
}
