package me.sendow.speedrun;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Game;
import me.sendow.speedrun.model.CategoryModel;
import me.sendow.speedrun.service.CategoryService;
import me.sendow.speedrun.service.GameService;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@SessionScoped
public class EditCategoryBean implements Serializable {

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient GameService gameService;

    @Inject
    @SuppressWarnings("CdiUnproxyableBeanTypesInspection")
    private transient CategoryService categoryService;

    private Long id;
    private CategoryModel model;

    public String init(long categoryId) {
        final Category category = categoryService.findById(categoryId);
        id = category.getId();
        model = new CategoryModel();
        model.setName(category.getName());
        model.setRules(category.getRules());
        return "/category/edit?faces-redirect=true";
    }

    public String edit() {
        final Category category = categoryService.update(id, model.getName(), model.getRules());
        try {
            return String.format("/game?gameId=%d&categoryId=%d&faces-redirect=true", category.getGame().getId(), category.getId());
        } finally {
            id = null;
            model = null;
        }
    }

    public String delete() {
        final Game game = gameService.findByCategory(id);
        categoryService.delete(id);
        try {
            return String.format("/game?gameId=%d&faces-redirect=true", game.getId());
        } finally {
            id = null;
            model = null;
        }
    }

    public CategoryModel getModel() {
        return model;
    }

}
