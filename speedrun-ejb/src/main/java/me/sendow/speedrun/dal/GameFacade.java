package me.sendow.speedrun.dal;

import me.sendow.speedrun.entity.Game;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@LocalBean
public class GameFacade extends AbstractFacade<Game> {

    @PersistenceContext
    private EntityManager entityManager;

    public GameFacade() {
        super(Game.class);
    }

    @Override
    protected EntityManager em() {
        return entityManager;
    }

    public List<Game> findAllOrderByTitle() {
        return entityManager.createQuery("select g from Game g order by g.title", Game.class)
                .getResultList();
    }

    public Game findByCategoryId(long categoryId) {
        return entityManager.createQuery("select c.game from Category c where c.id = :categoryId", Game.class)
                .setParameter("categoryId", categoryId)
                .getSingleResult();
    }

}
