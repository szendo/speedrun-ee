package me.sendow.speedrun.dal;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Entry;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@LocalBean
public class EntryFacade extends AbstractFacade<Entry> {

    @PersistenceContext
    private EntityManager entityManager;

    public EntryFacade() {
        super(Entry.class);
    }

    @Override
    protected EntityManager em() {
        return entityManager;
    }

    public List<Entry> getEntriesForCategory(Category category) {
        return entityManager.createQuery("select e from Entry e where e.category = :category order by e.runTime asc", Entry.class)
                .setParameter("category", category)
                .getResultList();
    }

    public long getRankForEntry(Entry entry) {
        return entityManager.createQuery("select count(e) + 1 as rank from Entry e where e.category = :category and e.runTime < :runTime", Long.class)
                .setParameter("category", entry.getCategory())
                .setParameter("runTime", entry.getRunTime())
                .getSingleResult();
    }

    public Entry getEntryForPlayerInCategory(Category category, String username) {
        try {
            return entityManager.createQuery("select e from Entry e where e.category = :category and e.username = :username", Entry.class)
                    .setParameter("category", category)
                    .setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

}
