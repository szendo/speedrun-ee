package me.sendow.speedrun.dal;

import me.sendow.speedrun.entity.User;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
@LocalBean
public class UserFacade extends AbstractFacade<User> {

    @PersistenceContext
    private EntityManager entityManager;

    public UserFacade() {
        super(User.class);
    }

    @Override
    protected EntityManager em() {
        return entityManager;
    }

    public boolean isUsernameInUse(String username) {
        return entityManager.createQuery("select count(u) from User u where u.username = :username", Long.class)
                .setParameter("username", username)
                .getSingleResult() > 0;
    }

}
