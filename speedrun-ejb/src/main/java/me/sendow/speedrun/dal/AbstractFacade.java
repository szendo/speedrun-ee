package me.sendow.speedrun.dal;

import javax.persistence.EntityManager;

public abstract class AbstractFacade<T> {
    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager em();

    public T findById(long id) {
        return em().find(entityClass, id);
    }

    public T getReference(long id) {
        return em().getReference(entityClass, id);
    }

    public T create(T entity) {
        em().persist(entity);
        return entity;
    }

    public void delete(T entity) {
        em().remove(entity);
    }

}
