package me.sendow.speedrun.dal;

import me.sendow.speedrun.entity.Category;
import me.sendow.speedrun.entity.Game;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
@LocalBean
public class CategoryFacade extends AbstractFacade<Category> {

    @PersistenceContext
    private EntityManager entityManager;

    public CategoryFacade() {
        super(Category.class);
    }

    @Override
    protected EntityManager em() {
        return entityManager;
    }

    public List<Category> getCategoriesForGame(Game game) {
        return entityManager.createQuery("select c from Category c where c.game = :game order by c.name", Category.class)
                .setParameter("game", game)
                .getResultList();
    }

    public Category findByEntryId(long entryId) {
        return entityManager.createQuery("select e.category from Entry e where e.id = :entryId", Category.class)
                .setParameter("entryId", entryId)
                .getSingleResult();
    }

}
