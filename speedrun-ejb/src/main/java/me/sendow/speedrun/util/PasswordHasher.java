package me.sendow.speedrun.util;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

@Stateless
@LocalBean
public class PasswordHasher {

    private final MessageDigest sha256Digest;
    private final Base64.Encoder base64Encoder;

    public PasswordHasher() {
        try {
            sha256Digest = MessageDigest.getInstance("SHA-256");
            base64Encoder = Base64.getEncoder();
        } catch (NoSuchAlgorithmException e) {
            // Every implementation of the Java platform is required to support SHA-256
            throw new RuntimeException(e);
        }
    }

    public String getPasswordHash(String password) {
        return base64Encoder.encodeToString(sha256Digest.digest(password.getBytes(StandardCharsets.UTF_8)));
    }

}
