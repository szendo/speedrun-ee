package me.sendow.speedrun.service;

import me.sendow.speedrun.dal.UserFacade;
import me.sendow.speedrun.entity.User;
import me.sendow.speedrun.util.PasswordHasher;

import javax.annotation.security.PermitAll;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Collections;

@Stateless
@LocalBean
public class UserService {

    @Inject
    private UserFacade userFacade;

    @Inject
    private PasswordHasher passwordHasher;

    @PermitAll
    public User create(String username, String password) {
        if (!userFacade.isUsernameInUse(username)) {
            final User user = new User();
            user.setUsername(username);
            user.setPassword(passwordHasher.getPasswordHash(password));
            user.setRoles(Collections.singleton("user"));
            return userFacade.create(user);
        } else {
            return null;
        }
    }

}
