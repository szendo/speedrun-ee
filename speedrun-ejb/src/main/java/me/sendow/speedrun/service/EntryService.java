package me.sendow.speedrun.service;

import me.sendow.speedrun.dal.CategoryFacade;
import me.sendow.speedrun.dal.EntryFacade;
import me.sendow.speedrun.entity.Entry;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class EntryService {

    @Inject
    private CategoryFacade categoryFacade;

    @Inject
    private EntryFacade entryFacade;

    public EntryService() {
    }

    @PermitAll
    public List<Entry> listForCategory(long categoryId) {
        return entryFacade.getEntriesForCategory(categoryFacade.getReference(categoryId));
    }

    @PermitAll
    public Entry findById(long id) {
        return entryFacade.findById(id);
    }

    @RolesAllowed({"user", "admin"})
    public Entry findByUsernameAndCategory(long categoryId, String username) {
        return entryFacade.getEntryForPlayerInCategory(categoryFacade.getReference(categoryId), username);
    }

    @PermitAll
    public long getRank(long id) {
        return entryFacade.getRankForEntry(entryFacade.getReference(id));
    }

    @RolesAllowed({"user", "admin"})
    public Entry createForCategory(Entry entry, long categoryId) {
        entry.setCategory(categoryFacade.getReference(categoryId));
        return entryFacade.create(entry);
    }

    @RolesAllowed({"user", "admin"})
    public Entry update(long id, long runTime, String videoUrl, String comment) {
        final Entry entry = entryFacade.findById(id);
        entry.setRunTime(runTime);
        entry.setVideoUrl(videoUrl);
        entry.setComment(comment);
        return entry;
    }

    @RolesAllowed("admin")
    public void delete(long id) {
        entryFacade.delete(entryFacade.getReference(id));
    }

}
