package me.sendow.speedrun.service;

import me.sendow.speedrun.dal.CategoryFacade;
import me.sendow.speedrun.dal.GameFacade;
import me.sendow.speedrun.entity.Category;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class CategoryService {

    @Inject
    private GameFacade gameFacade;

    @Inject
    private CategoryFacade categoryFacade;

    public CategoryService() {
    }

    @PermitAll
    public List<Category> listForGame(long gameId) {
        return categoryFacade.getCategoriesForGame(gameFacade.getReference(gameId));
    }

    @PermitAll
    public Category findById(long id) {
        return categoryFacade.findById(id);
    }

    @PermitAll
    public Category findByEntry(long entryId) {
        return categoryFacade.findByEntryId(entryId);
    }

    @RolesAllowed({"user", "admin"})
    public Category createForGame(Category category, long gameId) {
        category.setGame(gameFacade.getReference(gameId));
        return categoryFacade.create(category);
    }

    @RolesAllowed("admin")
    public Category update(long id, String name, String rules) {
        final Category category = categoryFacade.findById(id);
        category.setName(name);
        category.setRules(rules);
        return category;
    }

    @RolesAllowed("admin")
    public void delete(long id) {
        categoryFacade.delete(categoryFacade.getReference(id));
    }

}
