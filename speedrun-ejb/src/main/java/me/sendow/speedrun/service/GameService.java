package me.sendow.speedrun.service;

import me.sendow.speedrun.dal.GameFacade;
import me.sendow.speedrun.entity.Game;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
@LocalBean
public class GameService {

    @Inject
    private GameFacade gameFacade;

    public GameService() {
    }

    @PermitAll
    public List<Game> listAll() {
        return gameFacade.findAllOrderByTitle();
    }

    @PermitAll
    public Game findById(long id) {
        return gameFacade.findById(id);
    }

    @PermitAll
    public Game findByCategory(long categoryId) {
        return gameFacade.findByCategoryId(categoryId);
    }

    @RolesAllowed({"user", "admin"})
    public Game create(Game game) {
        return gameFacade.create(game);
    }

    @RolesAllowed("admin")
    public Game update(long id, String title, byte[] imageData) {
        final Game game = gameFacade.findById(id);
        game.setTitle(title);
        if (imageData != null) {
            game.setImage(imageData);
        }
        return game;
    }

    @RolesAllowed("admin")
    public void delete(long id) {
        gameFacade.delete(gameFacade.getReference(id));
    }

}
